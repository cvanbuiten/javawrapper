Author: Carlo van Buiten
Date: 15/11/2020

##Java classifier for histological response in Eosinophilic Esophagitis (EoE) patients.

###Introduction  
This program uses a OneR weka model to classify data from people suffering of EoE.

###Prerequisites  
Java 14.0.2

###Dependencies  
nz.ac.waikato.cms.weka (Weka)  
commons-cli:commons-cli:1.4 (Apache CLI)

###Installation  
Simply clone the source files either by typing "git clone https://cvanbuiten@bitbucket.org/cvanbuiten/javawrapper.git"
in your terminal or by manually downloading the files.

###Usage  
The directory build/libs contains the javawrapper-1.0-SNAPSHOT.jar. Run this jar on the command line as follows:  
java -jar <(path/to/)javawrapper-1.0-SNAPSHOT.jar> -f <(path/to/)data.arff>

The input data that is to be classified needs to at least contain the following attributes (testdata.arff is included, but not in the online repository due to privacy reasons.):  
neocate {0,1}  
ekcal.sixwk.vitc NUMERIC  
ekcal.sixwk.folate NUMERIC  
ekcal.sixwk.nicotinacid NUMERIC  
ekcal.sixwk.folateequiv NUMERIC

###Troubleshooting  
For more information you can email the author at:  
c.van.buiten@st.hanze.nl or carlobuiten@hotmail.com